import numpy as np
#import pandas as pd
import h5py
from keras.models import Sequential
from keras.layers import Dense
#from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
#from sklearn.model_selection import cross_val_score
#from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
#from sklearn.pipeline import Pipeline
import simplejson

seed = 7
np.random.seed(seed)

########## load dataset #####################
# input dataset
input_data = h5py.File('3_type.h5', 'r')
output_type = open("output_data.txt", "r")

# input data from hdf5 file and slice it each array
X = input_data['the_data'][:]
# output data
#Y = ["aircon", "aircon", "aircon", "CFL", "CFL", "CFL", "fan", "fan", "fan"]
Y = output_type.read().split(',')
########## load dataset #####################

encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)

dummy_y = np_utils.to_categorical(encoded_Y)

model = Sequential()
model.add(Dense(500, input_dim=500, init='normal', activation='relu'))
model.add(Dense(100, init='normal', activation='relu'))
model.add(Dense(60, init='normal', activation='relu'))
model.add(Dense(3, init='normal', activation='sigmoid'))


model.compile(loss='categorical_crossentropy', optimizer='adamax', metrics=['accuracy'])

model.fit(X, dummy_y, nb_epoch=500, batch_size=10)

scores = model.evaluate(X, dummy_y)

print("%s: %.2f%% , %s: %.2f%%" % (model.metrics_names[0], scores[0]*100, model.metrics_names[1], scores[1]*100))
#f = open('history.txt', 'w')
#save = (history.history)
#simplejson.dump(save,f)
#print save
#f.close()


